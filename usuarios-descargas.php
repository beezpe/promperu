<?php 
    session_start();

    if (!isset($_SESSION['email'])) {
        header('Location: ./');
        die();
    }

    include 'config.php';
    
    try {
       $conexion = mysqli_connect('localhost', $usuario, $contrasena, $dbname);
    } catch (PDOException $e) {
        echo "Error:" . $e->getMessage();
    } 


    
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="encoding" charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html">
        <meta name="robots" content="all, index, follow">
        <meta name="googlebot" content="all, index, follow">
        <meta http-equiv="cache-control" content="no-cache">
        <meta property="og:type" content="website">
        <meta name="twitter:card" content="summary_large_image">
        <meta property="og:locale" content="es_ES">
        <meta property="og:locale:alternate" content="es_ES">
        <meta http-equiv="content-language" content="es_ES">
        <meta name=apple-mobile-web-app-capable content="yes">
        <meta name="owner" content="Beez.pe">
        <meta name="author" content="Beez.pe">
        <meta name="publisher" content="Beez.pe">
        <meta name="copyright" content="Beez.pe">
        <meta name="twitter:creator" content="Beez.pe">
        <meta name="twitter:site" content="Beez.pe">
        <meta name="generator" content="Beez.pe">
        <meta name="organization" content="Beez.pe">
        <meta property="og:image:width" content="598">
        <meta property="og:image:height" content="274">
        <link rel="icon" sizes="192x192" href="img/home/favicon.jpg">
        <link rel="icon" sizes="32x32" href="img/home/favicon.jpg">
        <link rel="stylesheet" href="">
        <meta property="twitter:image" content="img/home/favicon.jpg">
        <meta property="og:image" content="img/home/favicon.jpg">
        <meta property="og:image:secure_url" content="img/home/favicon.jpg">
        <link rel="apple-touch-icon-precomposed" href="img/home/favicon.jpg">
        <meta name="msapplication-TileImage" content="img/home/favicon.jpg">
        <link rel="icon" type="image/jpg" href="img/home/favicon.jpg">
        <meta property="og:url" content="url">
        <meta property="og:site_name" content="url">
        <meta property="twitter:url" content="url">
        <link rel="canonical" href="url">
        <link rel="shortlink" href="url">
        <link rel="dns-prefetch" href="//s.w.org">
        <link rel="dns-prefetch" href="//fonts.googleapis.com">
        <link href="https://fonts.gstatic.com" crossorigin rel="preconnect">
        <meta name="description"
            content="Evento especializado que reúne a lo mejor de la oferta de contenidos y soluciones empresariales del sector de servicios, convirtiéndola en la principal plataforma de negocios para el comercio en la región Latinoamericana.">
        <meta name="twitter:description"
            content="Evento especializado que reúne a lo mejor de la oferta de contenidos y soluciones empresariales del sector de servicios, convirtiéndola en la principal plataforma de negocios para el comercio en la región Latinoamericana.">
        <meta property="og:description"
            content="Evento especializado que reúne a lo mejor de la oferta de contenidos y soluciones empresariales del sector de servicios, convirtiéndola en la principal plataforma de negocios para el comercio en la región Latinoamericana.">
        <meta name="keywords"
            content="Evento especializado que reúne a lo mejor de la oferta de contenidos y soluciones empresariales del sector de servicios, convirtiéndola en la principal plataforma de negocios para el comercio en la región Latinoamericana.">
        <meta property="og:title" content="PROMPERU - Foro Internacional Virtual">
        <meta name="twitter:title" content="PROMPERU - Foro Internacional Virtual">
        <meta name="dcterms.title" content="PROMPERU - Foro Internacional Virtual">
        <meta name="DC.title" content="PROMPERU - Foro Internacional Virtual">
        <meta name="application-name" content="PROMPERU - Foro Internacional Virtual">
        <meta name="title" content="PROMPERU - Foro Internacional Virtual">
        <title>PROMPERU - Reportes</title>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;900&display=swap" rel="stylesheet">
        <!-- Bootstrap CSS File -->
        <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/pages/core2.css">
        <link rel="stylesheet" href="css/pages/dashboard.css">
        
        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '183609552241898');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=183609552241898&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
        
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178260177-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-178260177-1');
        </script>


    </head>
    <body id="site" class="site <?= isset($_SESSION['email']) ? 'logged ' : ''; ?>">

        <input type="hidden" id="usuarioLogeado" value="<?= isset($_SESSION['email']) ? $_SESSION['email'] : ''; ?>">

        <header class="header">
            <div class="max-content">
                <a class="header-content" href="/">
                    <span class="header-content-img"><img src="img/elementos/logotype.png" alt=""> </span>
                    <span class="header-content-img"><img src="img/elementos/LOGO peru 1.png" alt=""></span>
                </a>
                
                <div class="profile-button <?= isset($_SESSION['email']) ? 'logged' : ''; ?> ">
                    <a id="<?= isset($_SESSION['email']) ? 'cerrarSesion' : 'modaIniciarSesion'; ?>"  class="button button-send  " >
                        <img src="img/icons/user 1.svg" class="icon" /> ADMIN
                    </a>
                </div>
            </div>
            
        </header>
        
        <main id="site-main" class="site-main cleaner">


            

            <div class="dashboard-content">
                
                <div class="max-content">
                    <h1 class="dashboard-title">Bienvenido ADMIN</h1>
                     <h2 class="dashboard-subtitle">Este es un reporte de <span class="page-title">los usuarios que descargaron</span> hasta hoy. Hora de actualización:   <a href="javascript:void(0)" title="Refrescar datos" id="refresPage" class="content-refresh"><strong id="hora"> </strong>  <img src="img/icons/refresh.png" class="button-refresh"> </a> </h2>


                    <div class="m-t-40 content-buttons">
                          <div id="export"  class=" dashboard-download"> <img src="img/icons/excel.svg" class=" dashboard-download-icon">  Descargar </div>
                    </div>

                     <table id="table2excel" class="dashboard-table table  table-striped table2excel table2excel_with_colors" data-tableName="Test Table 3">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombres</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $sql = "SELECT * FROM registro_web";
                                $resultado = mysqli_query($conexion, $sql);
                                $count = 0;
                                while($mostrar = mysqli_fetch_array($resultado)){
                                     $count++; 
                            ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $mostrar['nombres'] ?></td>
                                    <td><?php echo $mostrar['email'] ?></td>
                                </tr>
                            <?php }?>
                        </tbody>
                    </table>


                </div>  


               


            </div>


        </main>
        
        <footer id="footer" class="section-bg">
            <div class="footer-top">

                

                <div class="footer-social">
                    <div class="container">
                    <div class="footer-info">
                    <h3>Síguenos en</h3>
                </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <a target="_blank" class="footer-social-content" href="https://www.facebook.com/promperu">
                                    <div class="footer-links">
                                        <p> <img src="img/icons/fb.svg" style="width: 19px" class="icon" />&nbsp <span class="footer-links-social"><i>@</i>promperu</span> en Facebook</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <a target="_blank" class="footer-social-content" href="https://twitter.com/PromPeru">
                                    <div class="footer-links">
                                        <p> <img src="img/icons/twitter.svg" style="width: 30px" class="icon" /> &nbsp<span class="footer-links-social"><i>@</i>promperu</span> en Twitter</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <a target="_blank" class="footer-social-content" href="https://www.youtube.com/channel/UCveRrj9wB1L5BOQV-9pvqfQ">
                                    <div class="footer-links">
                                        <p> <img src="img/icons/youtube.svg" style="width: 30px" class="icon" /> &nbsp<span class="footer-links-social"><i>@</i>promperu</span> en Youtube</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <a target="_blank" class="footer-social-content" href="https://www.linkedin.com/company/promperu">
                                    <div class="footer-links">
                                        <p> <img src="img/home/linkedin-logo.png" style="width: 30px" class="icon" /> &nbsp<span class="footer-links-social"><i>@</i>promperu</span> en Linkedin</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                </div>

                
            </div>
            <div class="container">
                <div class="copyright">
                    Copyright. Todos los derechos reservados 2020
                </div>
            </div>
        </footer>


      


        <!-- #footer -->
        <script src="lib/jquery/jquery.min.js"></script>
        <script src="lib/export/excel.min.js"></script>
        <script type="text/javascript">

             let momentoActual = new Date() 
            let hora = momentoActual.getHours() 
            let minuto = momentoActual.getMinutes() 
           let  segundo = momentoActual.getSeconds() 
           let horaImprimible = hora + " : " + minuto + " : " + segundo 

            document.getElementById('hora').innerHTML  = horaImprimible


            $('#refresPage').on("click", function(){
                window.location.reload();
            })



            $("#export").click(function(){
                
                  $("#table2excel").table2excel({
                
                    // exclude CSS class
                
                    exclude:".noExl",
                
                    name:"UsuariosRegistrados",
                
                    filename:"SomeFile",//do not include extension
                
                    fileext:".xls" // file extension
                
                  });
                
                });

            
        </script>

        
    </body>
</html>