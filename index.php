<?php 
    session_start();
    include 'config.php';
    $conexion = mysqli_connect('localhost', $usuario, $contrasena, $dbname);

    $query = "SELECT nombre FROM paises";
    $resultado = mysqli_query($conexion, $query);

    $query2 = "SELECT nombre FROM departamentos";
    $resultado2 = mysqli_query($conexion, $query2);
    
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="encoding" charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html">
        <meta name="robots" content="all, index, follow">
        <meta name="googlebot" content="all, index, follow">
        <meta http-equiv="cache-control" content="no-cache">
        <meta property="og:type" content="website">
        <meta name="twitter:card" content="summary_large_image">
        <meta property="og:locale" content="es_ES">
        <meta property="og:locale:alternate" content="es_ES">
        <meta http-equiv="content-language" content="es_ES">
        <meta name=apple-mobile-web-app-capable content="yes">
        <meta name="owner" content="Beez.pe">
        <meta name="author" content="Beez.pe">
        <meta name="publisher" content="Beez.pe">
        <meta name="copyright" content="Beez.pe">
        <meta name="twitter:creator" content="Beez.pe">
        <meta name="twitter:site" content="Beez.pe">
        <meta name="generator" content="Beez.pe">
        <meta name="organization" content="Beez.pe">
        <meta property="og:image:width" content="598">
        <meta property="og:image:height" content="274">
        <link rel="icon" sizes="192x192" href="img/home/favicon.jpg">
        <link rel="icon" sizes="32x32" href="img/home/favicon.jpg">
        <link rel="stylesheet" href="">
        <meta property="twitter:image" content="img/home/favicon.jpg">
        <meta property="og:image" content="img/home/favicon.jpg">
        <meta property="og:image:secure_url" content="img/home/favicon.jpg">
        <link rel="apple-touch-icon-precomposed" href="img/home/favicon.jpg">
        <meta name="msapplication-TileImage" content="img/home/favicon.jpg">
        <link rel="icon" type="image/jpg" href="img/home/favicon.jpg">
        <meta property="og:url" content="url">
        <meta property="og:site_name" content="url">
        <meta property="twitter:url" content="url">
        <link rel="canonical" href="url">
        <link rel="shortlink" href="url">
        <link rel="dns-prefetch" href="//s.w.org">
        <link rel="dns-prefetch" href="//fonts.googleapis.com">
        <link href="https://fonts.gstatic.com" crossorigin rel="preconnect">
        <meta name="description"
            content="Evento especializado que reúne a lo mejor de la oferta de contenidos y soluciones empresariales del sector de servicios, convirtiéndola en la principal plataforma de negocios para el comercio en la región Latinoamericana.">
        <meta name="twitter:description"
            content="Evento especializado que reúne a lo mejor de la oferta de contenidos y soluciones empresariales del sector de servicios, convirtiéndola en la principal plataforma de negocios para el comercio en la región Latinoamericana.">
        <meta property="og:description"
            content="Evento especializado que reúne a lo mejor de la oferta de contenidos y soluciones empresariales del sector de servicios, convirtiéndola en la principal plataforma de negocios para el comercio en la región Latinoamericana.">
        <meta name="keywords"
            content="Evento especializado que reúne a lo mejor de la oferta de contenidos y soluciones empresariales del sector de servicios, convirtiéndola en la principal plataforma de negocios para el comercio en la región Latinoamericana.">
        <meta property="og:title" content="PROMPERU - Foro Internacional Virtual">
        <meta name="twitter:title" content="PROMPERU - Foro Internacional Virtual">
        <meta name="dcterms.title" content="PROMPERU - Foro Internacional Virtual">
        <meta name="DC.title" content="PROMPERU - Foro Internacional Virtual">
        <meta name="application-name" content="PROMPERU - Foro Internacional Virtual">
        <meta name="title" content="PROMPERU - Foro Internacional Virtual">
        <title>PROMPERU - Foro Internacional Virtual</title>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;900&display=swap" rel="stylesheet">
        <!-- Bootstrap CSS File -->
        <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/pages/core2.css">
        <link rel="stylesheet" href="lib/owlcarousel/assets/owl.carousel.min.css" type="text/css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css" rel="stylesheet" />
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" href="css/pages/registro2.css">

        
        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '183609552241898');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=183609552241898&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
        
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178260177-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-178260177-1');
        </script>


    </head>
    <body id="site" class="site <?= isset($_SESSION['email']) ? 'logged ' : ''; ?>">

        <input type="hidden" id="usuarioLogeado" value="<?= isset($_SESSION['email']) ? $_SESSION['email'] : ''; ?>">

        <header id="header">
            <div class="container">
                <div class="logo float-left">
                    <div class="text-light"><a href="#intro" class="scrollto">
                        <span><img src="img/elementos/logotype.png" alt=""> </span>
                        <span><img src="img/elementos/LOGO peru 1.png" alt=""></span></a>
                    </div>
                </div>
                <nav class="main-nav float-right main-menu">
                    <ul>
                        <li class="active"><a href="#intro">Inicio</a></li>
                        <li><a href="#about">Evento</a></li>
                        <li><a href="#program">Programa</a></li>
                        <li><a href="#participantes">Participantes</a></li>
                        <li><a href="#partners">Partners </a></li>
                        

                        <?= ((isset($_SESSION['email'])) && ($_SESSION['email'] == $admin1 || $_SESSION['email'] == $admin2 || $_SESSION['email'] == $admin3 || $_SESSION['email'] == $admin4 )) ? '<li><a href="usuarios-descargas.php">Descargas </a></li>': ''; ?>  
                        <?= ((isset($_SESSION['email'])) && ($_SESSION['email'] == $admin1 || $_SESSION['email'] == $admin2 || $_SESSION['email'] == $admin3 || $_SESSION['email'] == $admin4 )) ? '<li><a href="usuarios-registrados.php">Usuarios </a></li>': ''; ?>  
                        <li class="<?= isset($_SESSION['email']) ? 'logged' : ''; ?> ">
                            <a id="<?= isset($_SESSION['email']) ? 'cerrarSesion' : 'modaIniciarSesion'; ?>"  class="button button-send  " >
                                <?= isset($_SESSION['email']) ? '<img src="img/icons/user 1.svg" class="icon" /><span id="sessionUser"></span>' : 'Iniciar Sesión'; ?>
                            </a>
                        </li>
                    </ul>
                </nav>
                
                <nav class="main-nav float-right  main-nav--menu ">
                    <ul class="menu-mobile">
                        <li class="active menu-mobile-option">
                            <div class="menu-mobile-option-img">
                                <a href="#intro"><img src="img/home/home 1.png" /> </a>
                            </div>
                            <a href="#intro">  Inicio</a>
                        </li>
                        <li class="menu-mobile-option">
                            <div class="menu-mobile-option-img">
                                <a href="#about">
                                    <img src="img/home/ave 1.png" />
                                </a>
                            </div>
                            <a href="#about">Evento</a>
                         </li>
                         <li class="menu-mobile-option">
                            <div class="menu-mobile-option-img">
                                <a href="#program">
                                    <img src="img/home/pro 1.png" />
                                </a>
                            </div>
                             <a href="#program">Programa</a>
                        </li>
                        <li>
                            <div class="menu-mobile-option-img">
                                <a href="#participantes">
                                    <img src="img/home/participantes 1.png"/> 
                                </a>
                            </div>
                            <a href="#participantes">Participantes</a>
                        </li>
                        <li>
                            <div class="menu-mobile-option-img">
                                
                                <a href="#partners">
                                    <img src="img/home/manos 1.png"/> 
                                </a>
                            </div>
                            <a href="#partners">Partners </a>
                        </li>

                    </ul>
                </nav>
            </div>
        </header>
        <section id="intro" class="clearfix">
            <div id="particles-foreground" class="vertical-centered-box"></div>
            <div id="particles-background" class="vertical-centered-box"></div>


            <div  class="container d-flex h-100">
                <div id="contentMain" class="row justify-content-center align-self-center" style="
                    width: 100%;
                    ">
                    <div class="col-md-12 intro-info order-md-first order-last">
                        <h1 data-aos="zoom-in">Foro Internacional Virtual<br> <span>PERÚ SERVICE SUMMIT </span></h1>
                        <h2 class="ml12">Del 21 al 25 de Septiembre</h2>
                        <div class="content-buttons">
                            <a href="#program" class="button button-terciary">Ver programa </a>
                            

                            <?= isset($_SESSION['email']) ? '' : '<a id="modalRegistro" href="javascript:void(0)" class="button button-secondary">Registrarse </a>'; ?>
                        </div>
                        <div class="col-md-12 order-md-last order-first">
                            <div class="row justify-content-center align-self-center">
                                <div class="col-md-3 col-sm-6 icon-content text-center"
                                    data-aos-once="false"
                                    data-aos-duration="2000"
                                    data-aos-delay="400"
                                    data-aos="fade-up">
                                    <img src="img/elementos/v.png" alt="" class="img-fluid">
                                    <h4>23 Speakers Internacionales</h4>
                                </div>
                                <div class="col-md-3 col-sm-6 icon-content text-center" 
                                    data-aos-once="false"
                                    data-aos-duration="2000"
                                    data-aos-delay="500"
                                    data-aos="fade-up">
                                    <img src="img/elementos/z.png" alt="" class="img-fluid">
                                    <h4>8 Especialistas Nacionales</h4>
                                </div>
                                <div class="col-md-3 col-sm-6 icon-content text-center"
                                    data-aos-once="false"
                                    data-aos-duration="2000"
                                    data-aos-delay="600"
                                    data-aos="fade-up">
                                    <img src="img/elementos/n.png" alt="" class="img-fluid">
                                    <h4>6 Casos de Éxito </h4>
                                </div>
                                <div class="col-md-3 col-sm-6 icon-content text-center" 
                                    data-aos-once="false"
                                    data-aos-duration="2000"
                                    data-aos-delay="700"
                                    data-aos="fade-up">
                                    <img src="img/elementos/m.png" alt="" class="img-fluid">
                                    <h4>CEOs & Líderes de empresas de Servicios</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <main id="site-main" class="site-main cleaner">
            <section id="about">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-md-6">
                            <div class="about-img" data-aos="fade-right">
                                <img src="img/elementos/EVENTO 1.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6">
                            <div class="about-content">
                                <h2 data-aos-duration="1000"
                                    data-aos-delay="200"
                                    data-aos="fade-up"> Sobre el evento </h2>
                                <h3 data-aos-duration="1000"
                                    data-aos-delay="300"
                                    data-aos="fade-up">
                                    El PERÚ SERVICE SUMMIT es una de las principales plataformas comerciales para el comercio de servicios
                                    en Latinoamérica, por ello será el marco del lanzamiento de la marca sectorial de los Servicios
                                    Empresariales basados en Conocimiento que promueve
                                    PROMPERU y el desarrollo del Foro Internacional Virtual de Servicios.
                                </h3>
                                <br />
                                <h3 data-aos-duration="1000"
                                    data-aos-delay="400"
                                    data-aos="fade-up">
                                    El Foro es el componente académico del PSS, permite conocer las últimas tendencias e información del
                                    mercado para enfocar los objetivos de las empresas en los mercados internacionales.
                                </h3>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class=" row about-content">
                                <div class="col-md-7">
                                    <h4>Organizado por:</h4>
                                    <div class="organization">
                                        <img style="max-width: 235px;" src="img/home/PCM-Comercio-Exterior.png" />
                                        <img style="max-width: 140px" src="img/home/PROMPERU_logo.png" />
                                    </div>
                                </div>
                                <div class="col-md-5 ">
                                    <h4>Respaldado por:</h4>
                                    <img style="max-width: 180px" class="ion-android-checkmark-circle"
                                        src="img/home/Coalicion.jpeg" /></li>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- #about -->
            <section id="program" class="section-bg">
                <div class="container" data-aos="zoom-in">
                    <header class="section-header">
                        <h3 class="section-title">Programa</h3>
                    </header>
                    <div class="row">
                        <div class="col-lg-12">
                            <ul id="program-flters" class="nav nav-justified " role="tablist">
                                <li>
                                    <a class="active" id="21-tab-just" data-toggle="tab" href="#filter-21" role="tab"
                                        aria-controls="filter-21" aria-selected="true">Lunes 21</a>
                                </li>
                                <li>
                                    <a id="22-tab-just" data-toggle="tab" href="#filter-22" role="tab" aria-controls="filter-22"
                                        aria-selected="false">Martes 22</a>
                                </li>
                                <li>
                                    <a id="23-tab-just" data-toggle="tab" href="#filter-23" role="tab" aria-controls="filter-23"
                                        aria-selected="false">Miércoles 23</a>
                                </li>
                                <li>
                                    <a id="24-tab-just" data-toggle="tab" href="#filter-24" role="tab" aria-controls="filter-24"
                                        aria-selected="false">Jueves 24</a>
                                </li>
                                <li>
                                    <a id="25-tab-just" data-toggle="tab" href="#filter-25" role="tab" aria-controls="filter-25"
                                        aria-selected="false">Viernes 25</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row  tab-content row " id="myTabContentJust">
                        <div class="tab-pane fade show active row " id="filter-21" role="tabpanel" aria-labelledby="21-tab-just">
                            <div class=" col-md-3  ">
                                <div class=" member program-wrap">
                                    <img src="img/programa/lunes.jpg" class="img-fluid" alt="">
                                    
                                </div>
                                <div class="programa-registro">
                                    <button class="button button-detail" id="verLunesPrimero">Ver detalles</button>
                                </div>
                            </div>
                            <div class="col-md-3 program-item filter-21">
                                <h4>Inauguración. <br> Tendencias, políticas públicas y capital humano en el comercio internacional de servicios.</h4>
                                <p>8:30 a.m. a 12:00 p.m.</p>
                                <p>Modera: PromPerú</p>
                            </div>
                            
                        </div>
                        <!--  --22-- -->
                        <div class="tab-pane fade row" id="filter-22" role="tabpanel" aria-labelledby="22-tab-just">
                            <div class="col-md-3 program-item filter-21">
                                <div class="member program-wrap ">
                                    <img src="img/programa/martes.jpg" class="img-fluid" alt="">
                                    
                                </div>
                                <div class="programa-registro">
                                    <button class="button button-detail" id="verMartesPrimero">Ver detalles</button>
                                </div>
                            </div>
                            <div class="col-md-3 program-item filter-21">
                                <h4>Oportunidades para las empresas de Software.</h4>
                                <p>9:00 a.m. a 11:00 a.m.</p>
                                <p>Modera: APESOFT</p>
                            </div>
                        </div>
                        <!-- 23 -->
                        <div class="tab-pane fade row" id="filter-23" role="tabpanel" aria-labelledby="23-tab-just">
                            <div class=" col-md-3 program-item filter-23">
                                <div class=" member program-wrap">
                                    <img src="img/programa/miercoles2.jpg" class="img-fluid" alt="">
                                    
                                </div>
                                <div class="programa-registro">
                                    <button class="button button-detail" id="verMiercolesPrimero">Ver detalles</button>
                                </div>
                            </div>
                            <div class="col-md-3 program-item filter-23">
                                <h4>Oportunidades para las empresas de Fintech.</h4>
                                <p>9:00am – 11:00am</p>
                                <p>Modera: Fintech Perú </p>
                            </div>
                            <div class="col-md-3 program-item filter-23">
                                <div class="member program-wrap ">
                                    <img src="img/programa/miercoles1.jpg" class="img-fluid" alt="">
                                    
                                </div>
                                <div class="programa-registro">
                                    <button class="button button-detail" id="verMiercolesSegundo">Ver detalles</button>
                                </div>
                            </div>
                            <div class="col-md-3 program-item filter-23">
                                <h4>Oportunidades para las empresas de Cobranzas.</h4>
                                <p>5:00pm – 7:00pm</p>
                                <p>Modera: ANECOP</p>
                            </div>
                            
                        </div>

                        <div class="tab-pane fade row" id="filter-24" role="tabpanel" aria-labelledby="24-tab-just">
                            <div class=" col-md-3 program-item filter-24">
                                <div class=" member program-wrap">
                                    <img src="img/programa/jueves1.jpg" class="img-fluid" alt="">
                                    
                                </div>
                                <div class="programa-registro">
                                    <button class="button button-detail" id="verJuevesPrimero">Ver detalles</button>
                                </div>
                            </div>
                            <div class="col-md-3 program-item filter-24">
                                <h4>Oportunidades para las empresas de Diseño de branding.</h4>
                                <p>9:00am – 11:00am</p>
                                <p>Modera: ADÑ</p>
                            </div>
                            <div class="col-md-3 program-item filter-24">
                                <div class="member program-wrap ">
                                    <img src="img/programa/jueves2.jpg" class="img-fluid" alt="">
                                    
                                </div>
                                <div class="programa-registro">
                                    <button class="button button-detail" id="verJuevesSegundo">Ver detalles</button>
                                </div>
                            </div>
                            <div class="col-md-3 program-item filter-24">
                                <h4>Oportunidades para las empresas de Ingeniería.</h4>
                                <p>5:00pm – 7:00pm</p>
                                <p>Modera: APC</p>
                            </div>
                            
                        </div>


                        <div class="tab-pane fade row" id="filter-25" role="tabpanel" aria-labelledby="25-tab-just">
                            <div class=" col-md-3 program-item filter-25">
                                <div class=" member program-wrap">
                                    <img src="img/programa/viernes1.jpg" class="img-fluid" alt="">
                                    
                                </div>
                                <div class="programa-registro">
                                    <button class="button button-detail" id="verViernesPrimero">Ver detalles</button>
                                </div>
                            </div>
                            <div class="col-md-3 program-item filter-25">
                                <h4>Oportunidades para los Centros de Contactos.</h4>
                                <p>9:00am – 11:00am</p>
                                <p>Modera: APEXO</p>
                            </div>
                            <div class="col-md-3 program-item filter-25">
                                <div class="member program-wrap ">
                                    <img src="img/programa/viernes2.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="programa-registro">
                                    <button class="button button-detail" id="verViernesSegundo">Ver detalles</button>
                                </div>
                            </div>
                            <div class="col-md-3 program-item filter-25">
                                <h4>Oportunidades para las empresas de Animación.</h4>
                                <p>2:00pm – 4:00pm</p>
                                <p>Modera: Plan B</p>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>
            <!-- #program -->

         


            <section id="participantes" class="participantes ">
                <div class="container">

                    <h3 class="subtitle color-white">Participantes</h3>

                    <ul class="tabs-participantes">
                        <li class="tabs-participantes-option active" id="tabSpeaker">Speakers</li>
                        <li class="tabs-participantes-option" id="tabModerador">Moderadores</li>
                    </ul>

                    <div class="participantes-content" id="contentSpeakers">

                        <div class="owl-carousel owl-carousel2 participantes-speakers">                                
                            <div class="participantes-item">
                                <div class="participantes-item-person" style="background-image: url(img/fotos/GLORIA-URENA.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sra. Gloria Ureña</h4>
                                        <span class="participantes-enterprise">COLCOB</span>                           
                                    </div>
                                </div>
                                <div class="participantes-item-person" style="background-image: url(img/fotos/MAURO-CAMMA.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Mauro Cammá</h4>
                                        <span class="participantes-enterprise">CDU</span>                           
                                    </div>
                                </div>
                            </div>

                            <div class="participantes-item">
                                <div class="participantes-item-person" style="background-image: url(img/fotos/ROGELIO-SORTILLON.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Rogelio-Sortillon</h4>
                                        <span class="participantes-enterprise">APCOB</span>
                                    </div>
                                </div>
                                <div class="participantes-item-person" style="background-image: url(img/fotos/LUCHO-CORREA.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Lucho Correa</h4>
                                        <span class="participantes-enterprise">LIP</span>                           
                                    </div>
                                </div>
                            </div>

                            <div class="participantes-item">
                                <div class="participantes-item-person" style="background-image: url(img/fotos/ARIEL-REINHOLD.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Ariel Reinhold</h4>
                                        <span class="participantes-enterprise">ASARCOB</span>
                                    </div>
                                </div>
                                <div class="participantes-item-person" style="background-image: url(img/fotos/MARCELO-ROJAS.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Marcelo Rojas</h4>
                                        <span class="participantes-enterprise">CHILE DISEÑO</span>                           
                                    </div>
                                </div>
                            </div>

                            <div class="participantes-item">
                                <div class="participantes-item-person" style="background-image: url(img/fotos/MARCO-GUTIERREZ.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Marco Gutierrez</h4>
                                        <span class="participantes-enterprise">CNEC</span>
                                    </div>
                                </div>
                                <div class="participantes-item-person" style="background-image: url(img/fotos/HENRIQUE-DE-ARAGAO.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Henrique de Aragão</h4>
                                        <span class="participantes-enterprise">ABCE</span>                           
                                    </div>
                                </div>
                            </div>


                            <div class="participantes-item">
                                <div class="participantes-item-person" style="background-image: url(img/fotos/INIGO-ARRIBALZAGA.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Íñigo Arribalzaga</h4>
                                        <span class="participantes-enterprise">CEX</span>
                                    </div>
                                </div>
                                <div class="participantes-item-person" style="background-image: url(img/fotos/SONIA-BOIAROV.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sra. Sonia Boiarov</h4>
                                        <span class="participantes-enterprise">EXPERTA EN R.H.</span>                           
                                    </div>
                                </div>
                            </div>


                            <div class="participantes-item">
                                <div class="participantes-item-person" style="background-image: url(img/fotos/JORGE-MARIO-MARTINEZ.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Jorge Mario Martínez Piva</h4>
                                        <span class="participantes-enterprise">CEPAL</span>
                                    </div>
                                </div>
                                <div class="participantes-item-person" style="background-image: url(img/fotos/PEDRO-MIER.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Pedro Mier</h4>
                                        <span class="participantes-enterprise">AMETIC</span>
                                    </div>
                                </div>
                            </div>

                            <div class="participantes-item">
                                <div class="participantes-item-person" style="background-image: url(img/fotos/LUIS-TALLEDO.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sra. Luis Talledo</h4>
                                        <span class="participantes-enterprise">DELOITTE</span>                         
                                    </div>
                                </div>

                                
                                <div class="participantes-item-person" style="background-image: url(img/fotos/ERICK-RINCON.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Erick Rincón</h4>
                                        <span class="participantes-enterprise">COLOMBIA FINTECH</span>
                                    </div>
                                </div>
                            </div>

                            <div class="participantes-item">
                                <div class="participantes-item-person" style="background-image: url(img/fotos/JAVIER-ALLARD.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Javier Allard</h4>
                                        <span class="participantes-enterprise">AMITI</span>
                                    </div>
                                </div>
                                <div class="participantes-item-person" style="background-image: url(img/fotos/ANGEL-SIERRA.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Ángel Sierra</h4>
                                        <span class="participantes-enterprise">FINTECHILE</span>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="participantes-item">
                                <div class="participantes-item-person" style="background-image: url(img/fotos/SERGIO-CANDELO.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Sergio Candelo</h4>
                                        <span class="participantes-enterprise">CESSI</span>
                                    </div>
                                </div>
                                <div class="participantes-item-person" style="background-image: url(img/fotos/PAULO-DEITOS2.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Paulo Deitos</h4>
                                        <span class="participantes-enterprise">ABFINTECHS</span>                           
                                    </div>
                                </div>
                            </div>


                            <div class="participantes-item">
                                <div class="participantes-item-person" style="background-image: url(img/fotos/LAURA-GAGLIESI.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sra. Laura Gagliesi</h4>
                                        <span class="participantes-enterprise">CLUSTER ANIMAR</span>
                                    </div>
                                </div>
                                <div class="participantes-item-person" style="background-image: url(img/fotos/JOSE-INIESTA.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. José Inesta</h4>
                                        <span class="participantes-enterprise">PIXELATL</span>                           
                                    </div>
                                </div>
                            </div>
                            



                        </div>

                        <div class="my-owl-nav">
                            <span class="my-next-button">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22"><defs><clipPath><path fill="#00f" fill-opacity=".514" d="m-7 1024.36h34v34h-34z"/></clipPath><clipPath><path fill="#aade87" fill-opacity=".472" d="m-6 1028.36h32v32h-32z"/></clipPath></defs><path d="m345.44 248.29l-194.29 194.28c-12.359 12.365-32.397 12.365-44.75 0-12.354-12.354-12.354-32.391 0-44.744l171.91-171.91-171.91-171.9c-12.354-12.359-12.354-32.394 0-44.748 12.354-12.359 32.391-12.359 44.75 0l194.29 194.28c6.177 6.18 9.262 14.271 9.262 22.366 0 8.099-3.091 16.196-9.267 22.373" transform="matrix(.03541-.00013.00013.03541 2.98 3.02)" /></svg>
                            </span>
                            <span class="my-prev-button">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22"><defs><clipPath><path fill="#00f" fill-opacity=".514" d="m-7 1024.36h34v34h-34z"/></clipPath><clipPath><path fill="#aade87" fill-opacity=".472" d="m-6 1028.36h32v32h-32z"/></clipPath></defs><path d="m345.44 248.29l-194.29 194.28c-12.359 12.365-32.397 12.365-44.75 0-12.354-12.354-12.354-32.391 0-44.744l171.91-171.91-171.91-171.9c-12.354-12.359-12.354-32.394 0-44.748 12.354-12.359 32.391-12.359 44.75 0l194.29 194.28c6.177 6.18 9.262 14.271 9.262 22.366 0 8.099-3.091 16.196-9.267 22.373" transform="matrix(.03541-.00013.00013.03541 2.98 3.02)" /></svg>
                            </span>
                        </div>
                    </div>

                    <div class="participantes-content" id="contentModerador" style="display: none;">

                        <div class="owl-carousel owl-carousel3 participantes-speakers">                                
                            


                            <div class="participantes-item">
                                <div class="participantes-item-person" style="background-image: url(img/fotos/DAVID-EDERY.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. David Edery Muñoz</h4>
                                        <span class="participantes-enterprise">PROMPERÚ</span>
                                    </div>
                                </div>
                                <div class="participantes-item-person" style="background-image: url(img/fotos/ALEJANDRO-AGOIS.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Alejandro Agois</h4>
                                        <span class="participantes-enterprise">ADÑ</span>                           
                                    </div>
                                </div>
                            </div>

                            <div class="participantes-item">
                                <div class="participantes-item-person" style="background-image: url(img/fotos/JOSE-MORALES.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. José Morales</h4>
                                        <span class="participantes-enterprise">APESOFT</span>
                                    </div>
                                </div>
                                <div class="participantes-item-person" style="background-image: url(img/fotos/JAIME-SAAVEDRA.jpg">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Jaime Saavedra</h4>
                                        <span class="participantes-enterprise">APC</span>                           
                                    </div>
                                </div>
                            </div>

                            <div class="participantes-item">
                                <div class="participantes-item-person" style="background-image: url(img/fotos/KURT-GASTULO.jpg">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Kurt Gastulo</h4>
                                        <span class="participantes-enterprise">PLAN B</span>                           
                                    </div>
                                </div>
                                <div class="participantes-item-person" style="background-image: url(img/fotos/FRANCISCO-GRILLO.jpg">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Francisco Grillo</h4>
                                        <span class="participantes-enterprise">APEXO</span>                           
                                    </div>
                                </div>
                            </div>

                            <div class="participantes-item">
                                <div class="participantes-item-person" style="background-image: url(img/fotos/HUGO-TUESTA.jpg">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sr. Hugo Tuesta</h4>
                                        <span class="participantes-enterprise">ANECOP</span>                           
                                    </div>
                                </div>
                                <div class="participantes-item-person" style="background-image: url(img/fotos/MARIA-LAURA-CUYA.jpg)">
                                    <div class="participantes-item-info">                            
                                        <h4 class="participantes-name">Sra. Maria Laura Cuya</h4>
                                        <span class="participantes-enterprise">ASOCIACIÓN FINTECH DEL PERÚ</span>
                                    </div>
                                </div>
                            </div>







                        </div>

                        <div class="my-owl-nav">
                            <span class="my-next-button">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22"><defs><clipPath><path fill="#00f" fill-opacity=".514" d="m-7 1024.36h34v34h-34z"/></clipPath><clipPath><path fill="#aade87" fill-opacity=".472" d="m-6 1028.36h32v32h-32z"/></clipPath></defs><path d="m345.44 248.29l-194.29 194.28c-12.359 12.365-32.397 12.365-44.75 0-12.354-12.354-12.354-32.391 0-44.744l171.91-171.91-171.91-171.9c-12.354-12.359-12.354-32.394 0-44.748 12.354-12.359 32.391-12.359 44.75 0l194.29 194.28c6.177 6.18 9.262 14.271 9.262 22.366 0 8.099-3.091 16.196-9.267 22.373" transform="matrix(.03541-.00013.00013.03541 2.98 3.02)" /></svg>
                            </span>
                            <span class="my-prev-button">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22"><defs><clipPath><path fill="#00f" fill-opacity=".514" d="m-7 1024.36h34v34h-34z"/></clipPath><clipPath><path fill="#aade87" fill-opacity=".472" d="m-6 1028.36h32v32h-32z"/></clipPath></defs><path d="m345.44 248.29l-194.29 194.28c-12.359 12.365-32.397 12.365-44.75 0-12.354-12.354-12.354-32.391 0-44.744l171.91-171.91-171.91-171.9c-12.354-12.359-12.354-32.394 0-44.748 12.354-12.359 32.391-12.359 44.75 0l194.29 194.28c6.177 6.18 9.262 14.271 9.262 22.366 0 8.099-3.091 16.196-9.267 22.373" transform="matrix(.03541-.00013.00013.03541 2.98 3.02)" /></svg>
                            </span>
                        </div>
                    </div>
                
                </div>
            </section>



            <!-- #team -->
            <section id="partners" class="section-bg">
                <div class="container">
                    <header class="section-header">
                        <h3>Partners</h3>
                    </header>

                    <div class="slider-partner-desktop">
                        <div class="row" style="padding-top: 5%">
                            <div class="col-md-3 col-sm-6 box" 
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-03 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6  box" 
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/apesoft.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box" 
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/Logo ANECOP 2.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box" data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/1 4.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/deloitte 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/image 2.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/clientes/adn.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/LOGO APEXO 2.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-04 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/abf.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-08 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-13 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/cessi.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/image 4.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/clientes/cdu.png" /></div>
                            </div>
                            
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/ametic.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-05 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-10 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-14 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/image 3.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-06 1 (1).png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/clientes/cnec.jpg" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/clientes/animar.jpg" style="max-height: 80px;"/></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/fintech.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-07 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-12 1.png" /></div>
                            </div>
                        </div>
                    </div>


                    <div class="slider-partner slider-partner-mobile">
                    
                        <div class="row owl-carousel owl-carousel4 ">
                            <div class="col-md-3 col-sm-6 box" 
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-03 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6  box" 
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/apesoft.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box" 
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/Logo ANECOP 2.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box" data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/1 4.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/deloitte 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/image 2.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/clientes/adn.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/LOGO APEXO 2.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-04 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/abf.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-08 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-13 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/cessi.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/image 4.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/clientes/cdu.png" /></div>
                            </div>
                            
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/ametic.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-05 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-10 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-14 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/image 3.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-06 1 (1).png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/clientes/cnec.jpg" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/clientes/animar.jpg" style="max-height: 80px;"/></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/fintech.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-07 1.png" /></div>
                            </div>
                            <div class="col-md-3 col-sm-6 box"
                                data-aos-once="false"
                                data-aos-duration="1000"
                                data-aos-delay="100"
                                data-aos="fade-up">
                                <div class="icon"> <img src="img/elementos/PARTNERS-12 1.png" /></div>
                            </div>
                        </div>

                        <div class="partner-owl-nav">
                            <span class="partner-next-button">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22"><defs><clipPath><path fill="#00f" fill-opacity=".514" d="m-7 1024.36h34v34h-34z"/></clipPath><clipPath><path fill="#aade87" fill-opacity=".472" d="m-6 1028.36h32v32h-32z"/></clipPath></defs><path d="m345.44 248.29l-194.29 194.28c-12.359 12.365-32.397 12.365-44.75 0-12.354-12.354-12.354-32.391 0-44.744l171.91-171.91-171.91-171.9c-12.354-12.359-12.354-32.394 0-44.748 12.354-12.359 32.391-12.359 44.75 0l194.29 194.28c6.177 6.18 9.262 14.271 9.262 22.366 0 8.099-3.091 16.196-9.267 22.373" transform="matrix(.03541-.00013.00013.03541 2.98 3.02)" /></svg>
                            </span>
                            <span class="partner-prev-button">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22"><defs><clipPath><path fill="#00f" fill-opacity=".514" d="m-7 1024.36h34v34h-34z"/></clipPath><clipPath><path fill="#aade87" fill-opacity=".472" d="m-6 1028.36h32v32h-32z"/></clipPath></defs><path d="m345.44 248.29l-194.29 194.28c-12.359 12.365-32.397 12.365-44.75 0-12.354-12.354-12.354-32.391 0-44.744l171.91-171.91-171.91-171.9c-12.354-12.359-12.354-32.394 0-44.748 12.354-12.359 32.391-12.359 44.75 0l194.29 194.28c6.177 6.18 9.262 14.271 9.262 22.366 0 8.099-3.091 16.196-9.267 22.373" transform="matrix(.03541-.00013.00013.03541 2.98 3.02)" /></svg>
                            </span>
                        </div>
                    </div>
                    

                    <div class="row">
                        <div class="col-md-12 col-sm-12 book">
                            <div class="row" style="max-width: 768px;">
                                <div class="col-md-12 ">
                                    <h4>Los 100 casos de empresas de Servicios
                                        Empresariales Basados en Conocimiento
                                    </h4>
                                </div>
                                <div class="col-md-12 ">
                                    <form id="users" method="POST">
                                        <div class="row"> 
                                            <div class="col-md-4">
                                                <div class="form-field">
                                                    <input type="text" 
                                                    <?= isset($_SESSION['email']) ? 'disabled' : '' ; ?>
                                                    id="sessionName" class="form-input form-input--is-modal" name="nombres" id="nombres" placeholder="Nombres completos" required value="<?= isset($_SESSION['email']) ? '' : '' ; ?>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-field">
                                                    <input type="email" 
                                                    <?= isset($_SESSION['email']) ? 'disabled' : '' ; ?>
                                                    class="form-input form-input--is-modal" name="email" id="email" placeholder="Correo electrónico" required value="<?= isset($_SESSION['email']) ? $_SESSION['email'] : '' ; ?>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-field ">
                                                    <?= isset($_SESSION['email']) ? '<div class="text-center"><a target="_blank" href="https://drive.google.com/file/d/1bAmyAWtm_2Mm7FS4LJfZcDlrPEv26RpO/view" download="https://drive.google.com/file/d/1bAmyAWtm_2Mm7FS4LJfZcDlrPEv26RpO/view" class="button button-secondary">Descargar</a></div>' : '<div class="text-center"><button class="button button-secondary" id="btnguardar">Enviar</button></div>' ; ?>                                                    
                                                </div>
                                            </div>
                                            <div class="form-content-check <?= isset($_SESSION['email']) ? 'hidden' : '' ; ?>    ">
                                                <input class="form-check" type="checkbox" name="autorizacion" id="autorizacion" value="ok" required></input>
                                                <label for="autorizacion">Autorizo a que me envíen publicidad y/o promociones.</label>
                                            </div>
                                        </div>
                                    </form>                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- #partners -->
        </main>
        
        <footer id="footer" class="section-bg">
            <div class="footer-top">

                

                <div class="footer-social">
                    <div class="container">
                    <div class="footer-info">
                    <h3>Síguenos en</h3>
                </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <a target="_blank" class="footer-social-content" href="https://www.facebook.com/promperu">
                                    <div class="footer-links">
                                        <p> <img src="img/icons/fb.svg" style="width: 19px" class="icon" />&nbsp <span class="footer-links-social"><i>@</i>promperu</span> en Facebook</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <a target="_blank" class="footer-social-content" href="https://twitter.com/PromPeru">
                                    <div class="footer-links">
                                        <p> <img src="img/icons/twitter.svg" style="width: 30px" class="icon" /> &nbsp<span class="footer-links-social"><i>@</i>promperu</span> en Twitter</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <a target="_blank" class="footer-social-content" href="https://www.youtube.com/channel/UCveRrj9wB1L5BOQV-9pvqfQ">
                                    <div class="footer-links">
                                        <p> <img src="img/icons/youtube.svg" style="width: 30px" class="icon" /> &nbsp<span class="footer-links-social"><i>@</i>promperu</span> en Youtube</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <a target="_blank" class="footer-social-content" href="https://www.linkedin.com/company/promperu">
                                    <div class="footer-links">
                                        <p> <img src="img/home/linkedin-logo.png" style="width: 30px" class="icon" /> &nbsp<span class="footer-links-social"><i>@</i>promperu</span> en Linkedin</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                </div>

                
            </div>
            <div class="container">
                <div class="copyright">
                    Copyright. Todos los derechos reservados 2020
                </div>
            </div>
        </footer>


        <section id="contentModal" class="content-modal" style="display: none;">

                <modal class="modal-content modal-content--is-red">
                    <div id="closeModal" class="modal-close">
                        <img src="img/icons/close.svg">
                    </div>  
                    <section class="registro">
                        <nav class="registro-tabs">
                            <div id="registroUsuario" class="registro-tab-option active">Regístrate</div>
                            <div id="iniciarSesion" class="registro-tab-option">Iniciar sesión</div>
                            
                        </nav>

                        <form id="formRegistro" class="form-content form-content-registro" method="POST">
                            <div class="registro-content">              
                                <div class="row cleaner">
                                    <div class="col-md-6">
                                        <div class="form-field">
                                            <input type="text" autocomplete="nope" class="form-input form-input--is-modal" name="nombres" placeholder="Nombres completos (*)" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-field">
                                            <select name="pais" class="form-input form-input--is-modal" id="pais" name="pais" required>
                                                <option value="" disabled selected>País (*)</option>
                                                <?php 
                                                    while($paises = mysqli_fetch_array($resultado)){
                                                ?>
                                                <option value="<?php echo $paises['nombre']; ?>"> <?php echo $paises['nombre']; ?> </option>
                            
                                                <?php } ?>
                            
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6" id="contentDepartamento" style="display: none;">
                                        <div class="form-field">                                
                                            <select name="departamento" class="form-input form-input--is-modal" id="departamento">
                                                <option disabled selected>Departamento (*)</option>
                                                <?php 
                                                    while($departamentos = mysqli_fetch_array($resultado2)){
                                                ?>
                                                <option value="<?php echo $departamentos['nombre']; ?>"> <?php echo $departamentos['nombre']; ?> </option>

                                                <?php } ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-field">
                                            <input autocomplete="nope" type="text" class="form-input form-input--is-modal" id="empresa" name="empresa" placeholder="Empresa / Entidad (*)">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-field">
                                            <input autocomplete="nope" type="text" class="form-input form-input--is-modal" id="cargo" name="cargo" placeholder="Cargo (*)">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-field">
                                            <input type="email" autocomplete="nope" class="form-input form-input--is-modal" id="email" name="email" placeholder="Correo electrónico (*)" require>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-field">
                                            <input type="text" autocomplete="nope" class="form-input form-input--is-modal" id="telefono" name="telefono" placeholder="Teléfono (*)">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-field">
                                            <input autocomplete="nope" type="text" class="form-input form-input--is-modal" id="ruc" name="ruc" placeholder="Número de RUC">
                                        </div>
                                    </div>

                                </div>          
                            </div>

                            <div class="registro-tab-option m-t-20">Temas de interés (*)</div>
                            <div class="registro-temas">
                                <div class="row cleaner">
                                    <div class="col-md-6">
                                        <div class="form-content-check">
                                            <input class="form-check" type="checkbox" name="intereses[]" id="check1" value="Tendencias, políticas públicas y capital humano en el comercio internacional de servicios"></input>
                                            <label for="check1">Tendencias, políticas públicas y capital humano en el comercio internacional de servicios.</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-content-check">
                                            <input class="form-check" type="checkbox" name="intereses[]" id="check2" value="Oportunidades para las empresas de software"></input>
                                            <label for="check2">Oportunidades para las empresas de software.</label>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-content-check">
                                            <input class="form-check" type="checkbox" name="intereses[]" id="check3" value="Oportunidades para las empresas de Fintech Perú"></input>
                                            <label for="check3">Oportunidades para las empresas de Fintech Perú.</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-content-check">
                                            <input class="form-check" type="checkbox" name="intereses[]" id="check4" value="Oportunidades para las empresas de cobranzas"></input>
                                            <label for="check4">Oportunidades para las empresas de cobranzas.</label>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-content-check">
                                            <input class="form-check" type="checkbox" name="intereses[]" id="check5" value="Oportunidades para las empresas de diseño de branding"></input>
                                            <label for="check5">Oportunidades para las empresas de diseño de branding.</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-content-check">
                                            <input class="form-check" type="checkbox" name="intereses[]" id="check6" value="Oportunidades para las empresas de ingeniería"></input>
                                            <label for="check6">Oportunidades para las empresas de ingeniería.</label>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-content-check">
                                            <input class="form-check" type="checkbox" name="intereses[]" id="check7" value="Oportunidades para los centros de contacto"></input>
                                            <label for="check7">Oportunidades para los centros de contacto.</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-content-check">
                                            <input class="form-check" type="checkbox" name="intereses[]" id="check8" value="Oportunidades para las empresas de animación"></input>
                                            <label for="check8">Oportunidades para las empresas de animación.</label>
                                        </div>
                                    </div>


                                </div>
                            </div>


                            <div class="registro-tab-option m-t-20">Contraseña</div>
                            <div class="registro-login">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-field">
                                            <input type="password" class="form-input form-input--is-modal" id="password" name="password" placeholder="Ingresar contraseña" require>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-field">
                                            <input type="password" class="form-input form-input--is-modal" id="password2" name="password2" placeholder="Repetir contraseña" require>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-autorizacion ">
                                        <div class="form-content-check ">
                                            <input class="form-check" type="checkbox" name="terminos" id="check-terminos" value="ok" require></input>
                                            <label for="check-terminos">He leído las <a target="_blank" href="https://drive.google.com/file/d/1I36BOgPZc3EuyJASSI8iN5Z3CXTsC7uW/view?usp=sharing" class="link">políticas de privacidad</a></label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="m-t-20">
                                <button type="submit" class="button button-send" id="btnFormRegistro">Registrar</button>
                            </div>
                        </form>


                         <form id="formLogin" autocomplete="nope" autocomplete="false" class="form-content form-content-login" style="display: none">

                            <div class="registro-content"> 
                                <div class="row cleaner">
                                    <div class="col-md-6">
                                        <div class="form-field">
                                            <input type="email" autocomplete="nope" autocomplete="false" class="form-input form-input--is-modal" name="email" placeholder="Correo electrónico">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-field">
                                            <input type="password" autocomplete="nope" autocomplete="false" class="form-input form-input--is-modal" name="password" placeholder="Ingresar contraseña">
                                        </div>
                                    </div>

                                    
                                </div>          
                            </div>
                            
                            <style>
                                .recuperar-clave{
                                    display: flex;
                                    font-size: 18px;

                                }
                                .recuperar-clave-text{
                                    color: white
                                }
                                .recuperar-clave-url:hover{
                                    color: var(--color-secondary) !important
                                }
                                .recuperar-clave-url{
                                    text-decoration: underline !important;
                                    color: white !important;
                                    margin-left: 10px;
                                    cursor: pointer;
                                }
                            </style>
                            <div class="m-t-20 recuperar-clave">
                                <span class="recuperar-clave-text">¿No recuerdas tu contraseña?</span> <a class="recuperar-clave-url" id="recuperarPass" class="btn-recuperar-pass">Solicítala aquí</a>
                            </div>
                              
                            <div class="m-t-20">
                                <button class="button button-send" id="btnFormLogin">Iniciar sesión</button>
                            </div>
                           
                                
                        </form>
                        
                        <form id="formPass" class="form-content form-content-pass" method="POST" style="display: none">
                            <div class="registro-content">				
                                <div class="row cleaner">
                                    <div class="col-md-12">
                                        <h2 class="titulo-recupera-pass my-3">Ingresa tu correo y te haremos recuperar la clave de acceso</h2>
                                        <div class="form-field">
                                            <input type="email" class="form-input form-input--is-modal" name="email" placeholder="Correo electrónico" required>
                                        </div>
                                    </div>					
                                </div>			
                            </div>
                            <div class="m-t-20">
                                <button id="btnRecuperar" type="submit" class="button button-send">enviar</button>
                            </div>		
                        </form>

                    </section>

                </modal>
        </section>



        <section id="contentIframe" class="content-modal" style="display: none;" >
            <iframe id="iframe" src="" ></iframe>
        </section>



        <!-- #footer -->
        <script src="lib/jquery/jquery.min.js"></script>
        <script src="lib/easing/easing.min.js"></script>
        <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="lib/isotope/isotope.pkgd.min.js"></script>
        <script src="lib/aos/aos.min.js"></script>
        <script src="lib/animate/animate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js"></script>
        <script src="js/core/main.js"></script>
        <script src="js/core/register2.js"></script>
        <script src="js/core/particles.js"></script>
        <script src="js/core/carousel.js"></script>
    </body>
</html>