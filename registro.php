<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
	<meta name="encoding" charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html">

	<meta name="robots" content="all, index, follow">
	<meta name="googlebot" content="all, index, follow">
	<meta http-equiv="cache-control" content="no-cache">
	<meta property="og:type" content="website" >
	<meta name="twitter:card" content="summary_large_image">

	<meta property="og:locale" content="es_ES">
	<meta property="og:locale:alternate" content="es_ES">
	<meta http-equiv="content-language" content="es_ES">
	<meta name=apple-mobile-web-app-capable content="yes">

	<meta name="owner" content="Beez.pe">
	<meta name="author" content="Beez.pe">
	<meta name="publisher" content="Beez.pe">
	<meta name="copyright" content="Beez.pe">
	<meta name="twitter:creator" content="Beez.pe">	
	<meta name="twitter:site" content="Beez.pe">
	<meta name="generator" content="Beez.pe">
	<meta name="organization" content="Beez.pe">

	<meta property="og:image:width" content="598">
	<meta property="og:image:height" content="274">
	<link rel="icon" sizes="192x192" href="img/home/favicon.jpg">
	<link rel="icon" sizes="32x32" href="img/home/favicon.jpg">
	<link rel="stylesheet" href="">
	<meta property="twitter:image" content="img/home/favicon.jpg">
	<meta property="og:image" content="img/home/favicon.jpg">
	<meta property="og:image:secure_url" content="img/home/favicon.jpg">
	<link rel="apple-touch-icon-precomposed" href="img/home/favicon.jpg">
	<meta name="msapplication-TileImage" content="img/home/favicon.jpg">
	<link rel="icon" type="image/jpg" href= "img/home/favicon.jpg">

	<meta property="og:url" content="url">
	<meta property="og:site_name" content="url">
	<meta property="twitter:url" content="url">
	<link rel="canonical" href="url">
	<link rel="shortlink" href="url">
	<link rel="dns-prefetch" href="//s.w.org">
	<link rel="dns-prefetch" href="//fonts.googleapis.com">
	<link href="https://fonts.gstatic.com" crossorigin rel="preconnect">

	<meta name="description" content="Evento especializado que reúne a lo mejor de la oferta de contenidos y soluciones empresariales del sector de servicios, convirtiéndola en la principal plataforma de negocios para el comercio en la región Latinoamericana.">
	<meta name="twitter:description" content="Evento especializado que reúne a lo mejor de la oferta de contenidos y soluciones empresariales del sector de servicios, convirtiéndola en la principal plataforma de negocios para el comercio en la región Latinoamericana.">
	<meta property="og:description" content="Evento especializado que reúne a lo mejor de la oferta de contenidos y soluciones empresariales del sector de servicios, convirtiéndola en la principal plataforma de negocios para el comercio en la región Latinoamericana.">
	<meta name="keywords" content="Evento especializado que reúne a lo mejor de la oferta de contenidos y soluciones empresariales del sector de servicios, convirtiéndola en la principal plataforma de negocios para el comercio en la región Latinoamericana.">

	<meta property="og:title" content="PROMPERU - Foro Internacional Virtual">		
	<meta name="twitter:title" content="PROMPERU - Foro Internacional Virtual">
	<meta name="dcterms.title" content="PROMPERU - Foro Internacional Virtual">
	<meta name="DC.title" content="PROMPERU - Foro Internacional Virtual">
	<meta name="application-name" content="PROMPERU - Foro Internacional Virtual">
	<meta name="title" content="PROMPERU - Foro Internacional Virtual">
	<title>PROMPERU - Foro Internacional Virtual</title>

	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;900&display=swap" rel="stylesheet">
	<!-- Bootstrap CSS File -->
	<link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/pages/core.css">
    <link rel="stylesheet" href="css/pages/registro.css">
</head>

<body id="site" class="site">
	<?php 
	
	include 'conexion.php';
	$query = "SELECT nombre FROM paises";
	$resultado = mysqli_query($conexion, $query);

	$query2 = "SELECT nombre FROM departamentos";
	$resultado2 = mysqli_query($conexion, $query2);
	?>
    <modal class="modal-content modal-content--is-red">
    	<div class="modal-close">
    		<img src="img/icons/close.svg">
    	</div>	
    	<section class="registro">
			<nav class="registro-tabs">
				<div id="registroUsuario" class="registro-tab-option active"><a href="registro.php">Regístrate</a></div>
				<div id="iniciarSesion" class="registro-tab-option"><a href="login.php">Iniciar sesión</a></div>
			</nav>

			<form action="registro-modelo.php" class="form-content form-content-registro" method="POST">

				<div class="registro-content">				
					<div class="row cleaner">
						<div class="col-md-6">
							<div class="form-field">
								<input type="text" autocomplete="nope" class="form-input form-input--is-modal" name="nombres" placeholder="Nombres completos" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-field">
								<select name="pais" class="form-input form-input--is-modal" id="pais">
									<option value="" disabled selected>País</option>
									<?php 
										while($paises = mysqli_fetch_array($resultado)){
									?>
									<option value="<?php echo $paises['nombre']; ?>"> <?php echo $paises['nombre']; ?> </option>
				
									<?php }	?>
				
								</select>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-field">								
								<select name="departamento" class="form-input form-input--is-modal" id="departamento">
									<option disabled selected>Departamento</option>
									<?php 
										while($departamentos = mysqli_fetch_array($resultado2)){
									?>
									<option value="<?php echo $departamentos['nombre']; ?>"> <?php echo $departamentos['nombre']; ?> </option>

									<?php }	?>

								</select>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-field">
								<input autocomplete="nope" type="text" class="form-input form-input--is-modal" name="ruc" placeholder="Número de RUC">
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-field">
								<input autocomplete="nope" type="text" class="form-input form-input--is-modal" name="empresa" placeholder="Empresa / Entidad">
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-field">
								<input autocomplete="nope" type="text" class="form-input form-input--is-modal" name="cargo" placeholder="Cargo">
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-field">
								<input autocomplete="nope" type="email" class="form-input form-input--is-modal" name="email" placeholder="Correo electrónico" required>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-field">
								<input autocomplete="nope" type="text" class="form-input form-input--is-modal" name="telefono" placeholder="Teléfono">
							</div>
						</div>
					</div>			
				</div>

				<div class="registro-tab-option m-t-20">Temas de interés</div>
				<div class="registro-temas">
					<div class="row cleaner">
						<div class="col-md-6">
							<div class="form-content-check">
								<input class="form-check" type="checkbox" name="intereses[]" id="check1" value="Tendencias, politicas publicas y capital humano en el comercio internacional de servicios"></input>
								<label for="check1">Tendencias, políticas públicas y capital humano en el comercio internacional de servicios.</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-content-check">
								<input class="form-check" type="checkbox" name="intereses[]" id="check2" value="Oportunidades para las empresas de software"></input>
								<label for="check2">Oportunidades para las empresas de software.</label>
							</div>
						</div>


						<div class="col-md-6">
							<div class="form-content-check">
								<input class="form-check" type="checkbox" name="intereses[]" id="check3" value="Oportunidades para las empresas de Fintech Peru"></input>
								<label for="check3">Oportunidades para las empresas de Fintech Perú.</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-content-check">
								<input class="form-check" type="checkbox" name="intereses[]" id="check4" value="Oportunidades para las empresas de cobranzas"></input>
								<label for="check4">Oportunidades para las empresas de cobranzas.</label>
							</div>
						</div>


						<div class="col-md-6">
							<div class="form-content-check">
								<input class="form-check" type="checkbox" name="intereses[]" id="check5" value="Oportunidades para las empresas de diseño de branding"></input>
								<label for="check5">Oportunidades para las empresas de diseño de branding.</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-content-check">
								<input class="form-check" type="checkbox" name="intereses[]" id="check6" value="Oportunidades para las empresas de ingenieria"></input>
								<label for="check6">Oportunidades para las empresas de ingeniería.</label>
							</div>
						</div>


						<div class="col-md-6">
							<div class="form-content-check">
								<input class="form-check" type="checkbox" name="intereses[]" id="check7" value="Oportunidades para los centros de contacto"></input>
								<label for="check7">Oportunidades para los centros de contacto.</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-content-check">
								<input class="form-check" type="checkbox" name="intereses[]" id="check8" value="Oportunidades para las empresas de animacion"></input>
								<label for="check8">Oportunidades para las empresas de animación.</label>
							</div>
						</div>


					</div>
				</div>


				<div class="registro-tab-option m-t-20">Contraseña</div>
				<div class="registro-login">
					<div class="row">
						<div class="col-md-6">
							<div class="form-field">
								<input autocomplete="nope" type="text" class="form-input form-input--is-modal" name="password" placeholder="Ingresar contraseña" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-field">
								<input autocomplete="nope" type="text" class="form-input form-input--is-modal" name="password2" placeholder="Repetir contraseña" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-content-check">
								<input autocomplete="nope" class="form-check" type="checkbox" name="terminos" id="check-terminos" value="ok" required></input>
								<label for="check-terminos">He leído las <a href="javascript:void(0)" class="link">políticas de privacidad</a></label>
							</div>
						</div>
					</div>
				</div>

				<div class="m-t-20">
					<button class="button button-send" type="submit">Registrarme</button>
				</div>
				<?php if(!empty($errores)): ?>
				<div class="error">
					<ul>
						<?php echo $errores; ?>
					</ul>
				</div>
				<?php endif; ?>
			</form>
		
	    </section>

    </modal>


    <script src="lib/jquery/jquery.min.js"></script>
    <script src="js/core/register.js"></script>
	<script>
	
	$( function() {
    $("#pais").change( function() {
			if ($(this).val() === "Peru") {
				$("#departamento").prop("disabled", false);
			} else {
				$("#departamento").prop("disabled", true);
			}
		});
	});
	</script>
</body>
 </html>
