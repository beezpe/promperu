;(function () {

    'use strict';

    var carousels = function() {
       
        $(".owl-carousel2").owlCarousel(
            {
                loop:false,
                autoplay: false,
                center: false,
                autoplayTimeout: 2000,
                margin:20,
                nav:false,
                dots:false,
                autoplayHoverPause:true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:2
                    },
                    1000:{
                        items:3
                    },
                    1500:{
                        items:4
                    }
                }
            }
        );

        $(".owl-carousel3").owlCarousel(
            {
                loop:false,
                autoplay: false,
                center: false,
                autoplayTimeout: 2000,
                margin:20,
                nav:false,
                dots:false,
                autoplayHoverPause:true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:2
                    },
                    1000:{
                        items:3
                    },
                    1500:{
                        items:4
                    }
                }
            }
        );
    }



    var selector = $('.owl-carousel');

    $('.my-prev-button').click(function() {
      selector.trigger('next.owl.carousel');
    });

    $('.my-next-button').click(function() {
      selector.trigger('prev.owl.carousel');
    });


    // svg responsive in mobile mode
    var checkPosition = function() {
        if ($(window).width() < 767) {
            $("#bg-services").attr("viewBox", "0 0 1050 800");

        }
    };

    (function($) {
        carousels();
        checkPosition();
    })(jQuery);


}());

// menu toggle button
function myFunction(x) {
    x.classList.toggle("change");
}
