"use strict";


const APP = (function($, undefined) {   
    let events, suscribeEvents, beforeCatchDom, components, catchDom, dom, afterCatchDom, config, fn, initialize, st;
    st = {
       
    };
    dom = {};
    beforeCatchDom = () => {
        var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        if (isMobile) {
            

            $('.slider-partner-desktop').hide()


            $(".owl-carousel4").owlCarousel(
                {
                    loop:false,
                    autoplay: false,
                    center: true,
                    autoplayTimeout: 2000,
                    margin:20,
                    nav:false,
                    dots:false,
                    responsive:{
                        0:{
                            items:1
                        },
                        1000:{
                            items:1
                        }
                    }
                }
            );

            let navega = $('.owl-carousel4');

            $('.partner-prev-button').click(function() {
                navega.trigger('next.owl.carousel');
            });
            $('.partner-next-button').click(function() {
                navega.trigger('prev.owl.carousel');
            });
            
        }else{
            $('.owl-carousel4').removeClass('owl-carousel')
            $('.slider-partner-mobile').hide()
        }





        var session = $('#usuarioLogeado').val()

        if(session != ""){
            session = session.split("@")
            window.usuario = session[0]
            window.email = $('#usuarioLogeado').val()

            $('#sessionUser').text(window.usuario)
            $('#sessionName').val(window.usuario)
        }else{

        }



    };
    catchDom = () => {
        
    };
    afterCatchDom = () => {
       

    };
    suscribeEvents = () => {

        $('.recuperar-clave').on('click', events.showRecuperar)


        $('#registroUsuario').on('click', events.showRegister)
        $('#iniciarSesion').on('click', events.showLogin)
        $('#closeModal').on('click', events.closeModal)
        $('#modalRegistro').on('click', events.openModalRegistro)
        $('#modaIniciarSesion2').on('click', events.openModalIniciar)
        $('#modaIniciarSesion').on('click', events.openModalIniciar)
        $("#pais").on( "change" , function(){
            if ($(this).val() === "Peru") {
                $("#contentDepartamento").show()
            } else {
                $("#contentDepartamento").hide()
            }
        } )


        $('#btnguardar').on("click", events.downloadPdf )
        $('#btnFormRegistro').on("click", events.formRegister )
        $('#btnFormLogin').on("click", events.formLogin )


        $("#cerrarSesion").on("click", function(){
            Swal.fire({
              title: 'Cerrar Sesión',
              text: "¿Estás seguro de cerrar la sesión?",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#ffb81c',
              cancelButtonColor: '#3D4E54',
              confirmButtonText: 'Si, cerrarlo'
            }).then((result) => {
              if (result.isConfirmed) {
                window.location = "cerrar-sesion.php"
              }
            })
        })


        $('#verLunesPrimero').on('click', events.showLunesPrimero )
        $('#verMartesPrimero').on('click', events.showMartesPrimero )
        $('#verMiercolesPrimero').on('click', events.showMiercolesPrimero )
        $('#verMiercolesSegundo').on('click', events.showMiercolesSegundo )
        $('#verJuevesPrimero').on('click', events.showJuevesPrimero )
        $('#verJuevesSegundo').on('click', events.showJuevesSegundo )
        $('#verViernesPrimero').on('click', events.showViernesPrimero )
        $('#verViernesSegundo').on('click', events.showViernesSegundo )


        $('#btnRecuperar').on('click', events.formRecuperar )

    };
    events = {
        formRecuperar: () => {
            if($('#formPass input').val() != ""){
                var recuperar  = $('#formPass').serialize();
                $.ajax({
                    type: "POST",
                    url: "password-modelo.php",
                    data: recuperar ,
                    success:function(r){
                        if(r== "success"){
                            Swal.fire({
                              icon: 'success',
                              title: 'Correo enviado',
                              text: 'Verificar en tu correo la contraseña. Favor de revisar en la bandeja y el SPAM',
                            })
                        }else{
                            Swal.fire({
                              icon: 'error',
                              title: 'Hay problemas',
                              text: 'Verificar su usuario y/o contraseña',
                            })
                        }
                    }
                });

                return false;
            }else{
                Swal.fire({
                  icon: 'error',
                  title: 'Hay problemas',
                  text: 'Ingresar el correo',
                })
            }


            
        },
        showRecuperar:  () => {
            $('#formPass').show()
            $('#formLogin').hide()
        },
        showLunesPrimero : () => {

            $('#contentIframe').show()
            $('#iframe').attr("src", "html/lunes.php" )

            if(!$('body').hasClass('logged')){
                setTimeout(function(){
                    $("#iframe").contents().find("button").show()
                }, 1200)
            }            

        },
        showMartesPrimero : () => {

            $('#contentIframe').show()
            $('#iframe').attr("src", "html/martes.php" )

            if(!$('body').hasClass('logged')){
                setTimeout(function(){
                    $("#iframe").contents().find("button").show()
                }, 1200)
            }            

        },
        showMiercolesPrimero : () => {

            $('#contentIframe').show()
            $('#iframe').attr("src", "html/miercoles.php" )

            if(!$('body').hasClass('logged')){
                setTimeout(function(){
                    $("#iframe").contents().find("button").show()
                }, 1200)
            }            

        },
        showMiercolesSegundo : () => {

            $('#contentIframe').show()
            $('#iframe').attr("src", "html/miercoles2.php" )

            if(!$('body').hasClass('logged')){
                setTimeout(function(){
                    $("#iframe").contents().find("button").show()
                }, 1200)
            }            

        },
        showJuevesPrimero : () => {

            $('#contentIframe').show()
            $('#iframe').attr("src", "html/jueves.php" )

            if(!$('body').hasClass('logged')){
                setTimeout(function(){
                    $("#iframe").contents().find("button").show()
                }, 1200)
            }            

        },
        showJuevesSegundo : () => {

            $('#contentIframe').show()
            $('#iframe').attr("src", "html/jueves2.php" )

            if(!$('body').hasClass('logged')){
                setTimeout(function(){
                    $("#iframe").contents().find("button").show()
                }, 1200)
            }            

        },
        showViernesPrimero : () => {

            $('#contentIframe').show()
            $('#iframe').attr("src", "html/viernes.php" )

            if(!$('body').hasClass('logged')){
                setTimeout(function(){
                    $("#iframe").contents().find("button").show()
                }, 1200)
            }            

        },
        showViernesSegundo : () => {

            $('#contentIframe').show()
            $('#iframe').attr("src", "html/viernes2.php" )

            if(!$('body').hasClass('logged')){
                setTimeout(function(){
                    $("#iframe").contents().find("button").show()
                }, 1200)
            }            

        },
        formLogin: () => {

            var login = $('#formLogin').serialize();
            $.ajax({
                type: "POST",
                url: "login-modelo.php",
                data: login,
                success:function(r){
                    if(r== "success"){
                        window.location.reload();
                    }else{
                        Swal.fire({
                          icon: 'error',
                          title: 'Hay problemas',
                          text: 'Verificar su usuario y/o contraseña',
                        })
                    }
                }
            });

            return false;
        } ,
        formRegister: () => {

            
            $("#formRegistro").validate({
                rules: {
                    "nombres": {
                        required: true
                    },
                    "empresa": {
                        required: true
                    },
                    "pais": {
                        required: true
                    },
                    "cargo": {
                        required: true
                    },
                    "email": {
                        required: true,
                        email: true,
                        remote : {
                            url: 'validar-usuario-model.php',
                            type: "post",
                            data: {
                                email: function()
                                {
                                    return $('#formRegistro #email').val();
                                }
                            }
                        }
                    },
                    "telefono": {
                        required: true,
                        minlength: 6,
                        maxlength: 10
                    },
                    "ruc": {
                        maxlength: 15,
                        digits: true
                    },
                    "password": {
                        required: true,
                        minlength: 4
                    },
                    "password2": {
                        required: true,
                        equalTo: "#password"
                    },
                    "terminos": {
                        required: true
                    },
                    "intereses[]": {
                        required: true
                    }


                },
                messages: {
                    "nombres": {
                        required: "Campo obligatorio"
                    },
                    "empresa": {
                        required: "Campo obligatorio"
                    },
                    "pais": {
                        required: "Campo obligatorio"
                    },
                    "cargo": {
                        required: "Campo obligatorio"
                    },
                    "email": {
                        required: "Campo obligatorio",
                        email: "No es un formato de correo",
                        remote : "Este correo ya esta registrado"
                    },
                    "telefono": {
                        required: "Campo obligatorio",
                        minlength: "Mínimo 6 carateres",
                        maxlength: "Máximo 10 carateres"
                    },
                    "ruc": {
                        maxlength: "Máximo 15 caracteres",
                        digits: "Solo números"
                    },
                    "password": {
                        required: "Campo obligatorio",
                        minlength: "Mínimo 4 carateres"
                    },
                    "password2": {
                        required: "Campo obligatorio",
                        equalTo: "Las contraseñas no coinciden"
                    },
                    "terminos": {
                        required: "Campo obligatorio"
                    },
                    "intereses[]": {
                        required: "Seleccionar almenos 1 opción"
                    }

                },
                errorPlacement: function(error, element) {
                    if (element.attr("type") == "checkbox") {
                        if (element.attr("name") == "intereses[]") {
                            error.appendTo(".registro-temas");
                        } else{
                            error.appendTo(".form-autorizacion");
                        }
                   }
                    else {
                        error.insertAfter(element);
                    }
                    
                },
                submitHandler: function (form) { 

                    
                    
                    $.ajax({
                        type: "POST",
                        url: "registro-modelo.php",
                        data: $('#formRegistro').serialize(),
                        success:function(r){
                            if(r== "success"){
                                fbq('track', '[LEAD]TECHSTARSAREQUIPA');
                                Swal.fire({
                                  icon: 'success',
                                  title: 'Perfecto!',
                                  text: 'Usuario registado',
                                }).then(function() {
                                    window.location.reload();
                                });
                            }else{
                               Swal.fire({
                                  icon: 'error',
                                  title: 'Hay problemas',
                                  text: 'Es necesario completar todos los campos',
                                })
                            }
                        }
                    })


                    return false; 
                }
            });


            
        },
        downloadPdf: () => {

            var datos = $('#users').serialize();
            $.ajax({
                type: "POST",
                url: "usuarios-modelo.php",
                data: datos,
                success:function(r){
                    if(r==1){
                       Swal.fire({
                          icon: 'success',
                          title: 'Perfecto!',
                          text: 'Se  descargará un PDF. Disfrútalo!',
                        }).then(function() {
                            let url = 'https://drive.google.com/file/d/1bAmyAWtm_2Mm7FS4LJfZcDlrPEv26RpO/view'; 
                            window.open(url,'_blank'); 
                        });


                    }else{
                       Swal.fire({
                          icon: 'error',
                          title: 'Hay problemas',
                          text: 'Es necesario completar todos los campos',
                        })
                    }
                }
            });

            return false;
        },
        showRegister : (event) => {
            $('.form-content-registro').show()
            $('.form-content-login').hide()


            $(event.target).addClass("active")
            $('#iniciarSesion').removeClass("active")
            $("html").attr("style","overflow:hidden");
        },
        showLogin : (event) => {
            $('.form-content-login').show()
            $('.form-content-registro').hide()
            $('#formPass').hide()

            $(event.target).addClass("active")
            $('#registroUsuario').removeClass("active")
            $("html").attr("style","overflow:hidden");
        },
        closeModal: (event) => {
            $('#contentModal').hide();
            $("html").removeAttr('style')
        },
        openModalRegistro : (event) => {
            $('#registroUsuario').trigger('click')
            $('#contentModal').show()
            $("html").attr("style","overflow:hidden");
        },
        openModalIniciar : (event) => {
            $('#iniciarSesion').trigger('click')
            $('#contentModal').show();
            $("html").attr("style","overflow:hidden");

        }
    };
    fn = {
        loadProfile :  () => {            

            
        }
        
    };
    initialize = () => {
        beforeCatchDom();
        catchDom();
        afterCatchDom();
        suscribeEvents();        
    };
    return {
        init: initialize
    };
})(jQuery);
APP.init();


