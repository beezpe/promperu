<?php

    session_start();
	include 'config.php';

	$email= $_POST['email'];
	$password = $_POST['password'];
	$password = hash('sha512', $password);

	try {
		$conexion = new PDO($dns, $usuario, $contrasena);
	} catch (PDOException $e) {
		echo "Error:" . $e->getMessage();
	}

	$statement = $conexion->prepare('SELECT * FROM usuarios WHERE email = :email AND pass = :password');
	$statement->execute(array(
			':email' => $email,
			':password' => $password
		));

	$resultado = $statement->fetch();
	if ($resultado !== false) {
		$_SESSION['email'] = $email;
		echo "success";
	} else {
		$errores = '<li>Datos incorrectos</li>';
		header('Location: index.php');
	}


	



?>