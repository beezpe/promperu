<?php session_start();

    include 'config.php';
    

    $email = $_POST['email'];

    try {
        $conexion = new PDO($dns, $usuario, $contrasena);
    } catch (PDOException $e) {
        echo "Error:" . $e->getMessage();
    }

    $statement = $conexion->prepare('SELECT * FROM usuarios WHERE email = :email LIMIT 1');
    $statement->execute(array(':email' => $email));

    $resultado = $statement->fetch();

    if ($resultado != false) {
        echo "false";
    }else{
        echo "true";
    }

?>