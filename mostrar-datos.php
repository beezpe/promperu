<?php  
    try {
        $conexion = mysqli_connect('localhost', 'root', '', 'promperu');
    } catch (PDOException $e) {
        echo "Error:" . $e->getMessage();
    } 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <title>Document</title>
</head>
<body>
    
    <div class="container">
        <h2 class="text-center p-2 mt-3">Usuarios Registrados</h2>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombres</th>
                    <th scope="col">País</th>
                    <th scope="col">Departamento</th>
                    <th scope="col">Ruc</th>
                    <th scope="col">Empresa</th>
                    <th scope="col">Cargo</th>
                    <th scope="col">Email</th>
                    <th scope="col">Teléfono</th>
                    <th scope="col">Intereses</th>
                    <th scope="col">Términos</th>
                </tr>
            </thead>
            <?php 
                $sql = "SELECT * FROM usuarios";
                $resultado = mysqli_query($conexion, $sql);

                while($mostrar = mysqli_fetch_array($resultado)){
            ?>
                <tr>
                    <th scope="row"><?php echo $mostrar['id'] ?></td>
                    <td><?php echo $mostrar['nombres'] ?></td>
                    <td><?php echo $mostrar['pais'] ?></td>
                    <td><?php echo $mostrar['departamento'] ?></td>
                    <td><?php echo $mostrar['ruc'] ?></td>
                    <td><?php echo $mostrar['empresa'] ?></td>
                    <td><?php echo $mostrar['cargo'] ?></td>
                    <td><?php echo $mostrar['email'] ?></td>
                    <td><?php echo $mostrar['telefono'] ?></td>
                    <td><?php echo $mostrar['intereses'] ?></td>
                    <td><?php echo $mostrar['terminos'] ?></td>
                </tr>
            <?php }?>
        </table>
    </div>
    

</body>
</html>