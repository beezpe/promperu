<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
	<meta name="encoding" charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html">

	<meta name="robots" content="all, index, follow">
	<meta name="googlebot" content="all, index, follow">
	<meta http-equiv="cache-control" content="no-cache">
	<meta property="og:type" content="website" >
	<meta name="twitter:card" content="summary_large_image">

	<meta property="og:locale" content="es_ES">
	<meta property="og:locale:alternate" content="es_ES">
	<meta http-equiv="content-language" content="es_ES">
	<meta name=apple-mobile-web-app-capable content="yes">

	<meta name="owner" content="Beez.pe">
	<meta name="author" content="Beez.pe">
	<meta name="publisher" content="Beez.pe">
	<meta name="copyright" content="Beez.pe">
	<meta name="twitter:creator" content="Beez.pe">	
	<meta name="twitter:site" content="Beez.pe">
	<meta name="generator" content="Beez.pe">
	<meta name="organization" content="Beez.pe">

	<meta property="og:image:width" content="598">
	<meta property="og:image:height" content="274">
	<link rel="icon" sizes="192x192" href="img/home/favicon.jpg">
	<link rel="icon" sizes="32x32" href="img/home/favicon.jpg">
	<link rel="stylesheet" href="">
	<meta property="twitter:image" content="img/home/favicon.jpg">
	<meta property="og:image" content="img/home/favicon.jpg">
	<meta property="og:image:secure_url" content="img/home/favicon.jpg">
	<link rel="apple-touch-icon-precomposed" href="img/home/favicon.jpg">
	<meta name="msapplication-TileImage" content="img/home/favicon.jpg">
	<link rel="icon" type="image/jpg" href= "img/home/favicon.jpg">

	<meta property="og:url" content="url">
	<meta property="og:site_name" content="url">
	<meta property="twitter:url" content="url">
	<link rel="canonical" href="url">
	<link rel="shortlink" href="url">
	<link rel="dns-prefetch" href="//s.w.org">
	<link rel="dns-prefetch" href="//fonts.googleapis.com">
	<link href="https://fonts.gstatic.com" crossorigin rel="preconnect">

	<meta name="description" content="Evento especializado que reúne a lo mejor de la oferta de contenidos y soluciones empresariales del sector de servicios, convirtiéndola en la principal plataforma de negocios para el comercio en la región Latinoamericana.">
	<meta name="twitter:description" content="Evento especializado que reúne a lo mejor de la oferta de contenidos y soluciones empresariales del sector de servicios, convirtiéndola en la principal plataforma de negocios para el comercio en la región Latinoamericana.">
	<meta property="og:description" content="Evento especializado que reúne a lo mejor de la oferta de contenidos y soluciones empresariales del sector de servicios, convirtiéndola en la principal plataforma de negocios para el comercio en la región Latinoamericana.">
	<meta name="keywords" content="Evento especializado que reúne a lo mejor de la oferta de contenidos y soluciones empresariales del sector de servicios, convirtiéndola en la principal plataforma de negocios para el comercio en la región Latinoamericana.">

	<meta property="og:title" content="PROMPERU - Foro Internacional Virtual">		
	<meta name="twitter:title" content="PROMPERU - Foro Internacional Virtual">
	<meta name="dcterms.title" content="PROMPERU - Foro Internacional Virtual">
	<meta name="DC.title" content="PROMPERU - Foro Internacional Virtual">
	<meta name="application-name" content="PROMPERU - Foro Internacional Virtual">
	<meta name="title" content="PROMPERU - Foro Internacional Virtual">
	<title>PROMPERU - Foro Internacional Virtual</title>

	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;600;900&display=swap" rel="stylesheet">
	<!-- Bootstrap CSS File -->
	<link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/pages/core.css">
    <link rel="stylesheet" href="css/pages/registro.css">
</head>

<body id="site" class="site">
	
    <modal class="modal-content modal-content--is-red">
    	<div class="modal-close">
    		<img src="img/icons/close.svg">
    	</div>	
    	<section class="registro">
			<nav class="registro-tabs">
				<div id="registroUsuario" class="registro-tab-option"> <a href="registro.php">Regístrate</a> </div>
				<div id="iniciarSesion" class="registro-tab-option active"><a href="login.php">Iniciar sesión</a> </div>
			</nav>
			

			<form action="login-modelo.php" class="form-content form-content-login" method="POST">

				<div class="registro-content">				
					<div class="row cleaner">
						<div class="col-md-6">
							<div class="form-field">
								<input type="email" class="form-input form-input--is-modal" name="email" placeholder="Correo electrónico" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-field">
								<input type="text" class="form-input form-input--is-modal" name="password" placeholder="Ingresar contraseña" required>
							</div>
						</div>						
					</div>			
				</div>
				<div class="login-boton">
					<div class="m-t-20">
						<button type="submit" class="button button-send">Iniciar sesión</button>
					</div>
					<div class="m-t-20">
						<a href="recuperar-password.php" class="button button-send">¿Olvido su contraseña?</a>
					</div>
				</div>				
			</form>
			</form>
	    </section>

    </modal>


    <script src="lib/jquery/jquery.min.js"></script>
    <script src="js/core/register.js"></script>
</body>
 </html>
