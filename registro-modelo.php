<?php session_start();

include 'config.php';

if (isset($_SESSION['email'])) {
	header('Location: index.php');
	die();
}


	$nombres = $_POST['nombres'];
	$pais = $_POST['pais'];
	
	if(!empty($_POST['departamento'])) {
	   $departamento = $_POST['departamento'];
	}else{
		$departamento = "No hay";
	}

	if(!empty($_POST['ruc'])) {
		$ruc = $_POST['ruc'];
	 }else{
		 $ruc = "No hay";
	}


	
	$empresa = $_POST['empresa'];
	$cargo = $_POST['cargo'];
	$email = $_POST['email'];
	$telefono = $_POST['telefono'];
	$password = $_POST['password'];
	$password2 = $_POST['password2'];
	$terminos = $_POST['terminos'];


	$intereses = '';
	foreach ($_POST['intereses'] as $int){
		$s = '; ';
		if($intereses == ''){
			$intereses =$int;
		}else{
			$intereses .= $s.$int;
		}
	}

	$errores = '';

	if (empty($nombres) or empty($password) or empty($password2)) {
		$errores = '<li>Por favor rellena todos los datos correctamente</li>';
	} else {
		
		

		$password = hash('sha512', $password);
		$password2 = hash('sha512', $password2);

		if ($password != $password2) {
			echo "clavedif";
			exit;
		}
	}
	
	if ($errores == '') {

		try {
			$conexion = new PDO($dns, $usuario, $contrasena);

			} catch (PDOException $e) {
				echo "Error:" . $e->getMessage();
		}

		$statement = $conexion->prepare('INSERT INTO usuarios (id, nombres, pais, departamento, ruc, empresa, cargo, email, telefono, intereses, pass, terminos) VALUES (null, :nombres, :pais, :departamento, :ruc, :empresa, :cargo, :email, :telefono, :intereses, :pass, :terminos)');
		$statement->execute(array(
				':nombres' => $nombres,
				':pais' => $pais,
				':departamento' => $departamento,
				':ruc' => $ruc,
				':empresa' => $empresa,
				':cargo' => $cargo,
				':email' => $email,
				':telefono' => $telefono,
				':intereses' => $intereses,
				':pass' => $password,
				':terminos' => $terminos
			));

		$_SESSION['email'] = $email;
		echo "success";
	}else{
		

		echo "error";
	}

?>