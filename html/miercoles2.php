<!DOCTYPE html>
<html lang="es">
    <head>
        <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
         <link rel="stylesheet" href="../css/pages/core2.css" />
        <link rel="stylesheet" href="../css/pages/registro2.css" />
    </head>

    <body id="site" class="site">
        <modal id="modal-program" class="modal-content">
            <div id="closeModal" class="modal-close">
                <img src="../img/icons/close.svg" />
            </div>
            <section class="box">
                <h2>MIÉRCOLES</h2>
                <h3>PROGRAMA – DÍA 3 - 23 DE SEPTIEMBRE</h3>
                <div class="m-b-20">
                    <h4>APERTURA DEL FORO: 5:00 p.m</h4>
                    <h5>Palabras de bienvenida</h5>
                    <ul>
                        <li>• Hugo Tuesta, Presidente de ANECOP.</li>
                    </ul>
                </div>

                <div class="m-b-20">
                    <h4>BLOQUE DE PRESENTACIONES: 5:05 p.m.</h4>
                    <h5>Presentación del CASO de Éxito Peruano: Grupo Recupera.</h5>
                </div>

                <div class="m-b-20">
                    <h4>SESIÓN DEL PANEL: 5:25 p.m. – 7:00 p.m.</h4>
                    <h5>Moderación:</h5>
                    <ul>
                        <li>• Hugo Tuesta, Presidente de ANECOP.</li>
                    </ul>
                    <h5>Integrantes:</h5>
                    <ul>
                        <li>• Gloria Urueña, Gerente General de COLCOB.</li>
                        <li>• Rogelio Sortillon, Presidente de APCOB.</li>
                        <li>• Ariel Reinhold, Presidente de ASARCOB.</li>
                    </ul>
                    <h5>Temática:</h5>
                    <ul>
                        <li>• Análisis del contexto internacional y las brechas existentes en las empresas que prestan estos servicios desde la perspectiva de los países representados en el panel.</li>
                        <li>• Comentarios y citas de las normativas que están impulsando el desarrollo de estas actividades en sus países.</li>
                        <li>• Nuevas necesidades en el capital humano que requieren las empresas de este sector.</li>
                    </ul>
                </div>


                <div class="m-b-20">
                    <h4>CIERRE DE SESIÓN: 07:00 P.M.</h4>
                </div>


                <button class="button button-send" id="iframeRegistrar" style="display: none;">Registrar</button>
            </section>
        </modal>
    </body>
</html>

<script src="../lib/jquery/jquery.min.js"></script>
<script type="text/javascript">    
    document.getElementById("closeModal").onclick = function(){
        $('#site', window.parent.document).find('iframe').attr('src', '');
        $('#site', window.parent.document).find('#contentIframe').hide()
    }
    document.getElementById("iframeRegistrar").onclick = function(){  
        $('#site', window.parent.document).find('iframe').attr('src', '');
        $('#site', window.parent.document).find('#contentIframe').hide()
        $('#site', window.parent.document).find('#contentModal').show()
    }
</script>