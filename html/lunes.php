<!DOCTYPE html>
<html lang="es">
    <head>
        <link href="../lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="../css/pages/core2.css" />
        <link rel="stylesheet" href="../css/pages/registro2.css" />
    </head>

    <body id="site" class="site">
        <modal id="modal-program" class="modal-content">
            <div id="closeModal" class="modal-close">
                <img src="../img/icons/close.svg" />
            </div>
            <section class="box">
                <h2>LUNES</h2>
                <h3>PROGRAMA – DÍA 1 - 21 DE SEPTIEMBRE</h3>
                <div class="m-b-20">
                    <h4>INAUGURACIÓN DEL FORO: 8:30 a.m.</h4>
                    <h5>Palabras de bienvenida e inauguración:</h5>
                    <ul>
                        <li>• Presidente de PROMPERÚ .</li>
                        <li>• Ministra de Comercio Exterior y Turismo.</li>
                    </ul>
                </div>

                <div class="m-b-20">
                    <h4>9:20 a.m.</h4>
                    <h5>Tendencias del mercado y las brechas existentes en las empresas de servicios y políticas públicas para impulsar el sector</h5>
                    <ul>
                        <li>• Sr. Jorge Mario Martínez Piva, Jefe de la Unidad de Comercio Internacional e Industria, Sede Subregional en México, CEPAL.</li>
                    </ul>
                </div>

                <div class="m-b-20">
                    <h4>Competencias y capacidades del talento humano en el sector de servicios</h4>
                    <h5>Sr. Luis Talledo, Socio de Consultoría, Deloitte</h5>
                </div>

                <div class="m-b-20">
                    <h4>Moderación</h4>
                    <ul>
                        <li>• David Edery Muñoz, Coordinador del Departamento de Exportación de Servicios, PROMPERÚ.</li>
                    </ul>
                </div>

                <div class="m-b-20">
                    <h4>Ronda de preguntas y diálogo</h4>
                </div>

                
                <button class="button button-send" id="iframeRegistrar" style="display: none;">Registrar</button>
            </section>
        </modal>
    </body>
</html>

<script src="../lib/jquery/jquery.min.js"></script>
<script type="text/javascript">    
    document.getElementById("closeModal").onclick = function(){
        $('#site', window.parent.document).find('iframe').attr('src', '');
        $('#site', window.parent.document).find('#contentIframe').hide()
    }
    document.getElementById("iframeRegistrar").onclick = function(){  
        $('#site', window.parent.document).find('iframe').attr('src', '');
        $('#site', window.parent.document).find('#contentIframe').hide()
        $('#site', window.parent.document).find('#contentModal').show()
    }
</script>